﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace FinalExam.Services
{
    public class FileUploadService
    {
        private readonly IHostingEnvironment environment;

        public FileUploadService(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        public void Upload(string path, string fileName, IFormFile file)
        {
            try
            {
                Directory.CreateDirectory(path);
                using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void FileDelete(string path)
        {
            string fullPath = Path.Combine(environment.WebRootPath, path);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }

        public void AddPhoto(IFormFile file, string name)
        {
            string path = Path.Combine(
                environment.WebRootPath,
                $"images\\{name}");

            Upload(path, file.FileName, file);
        }
    }
}
