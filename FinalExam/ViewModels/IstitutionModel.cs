﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FinalExam.ViewModels
{
    public class IstitutionModel
    {
        [Required]
        public IFormFile MainPhoto { get; set; }
        [Required]
        public string Name { get; set; }
        public string UserId { get; set; }
        public string PhotoPath { get; set; }
        [Required]
        public string Description { get; set; }
        public int Rating { get; set; } = 5;
    }
}
