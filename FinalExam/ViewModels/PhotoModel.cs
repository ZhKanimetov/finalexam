﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;
using Microsoft.AspNetCore.Http;

namespace FinalExam.ViewModels
{
    public class PhotoModel : Picture
    {
        public IFormFile File { get; set; }
    }
}
