﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;

namespace FinalExam.ViewModels
{
    public class InfoModel
    {
        public Institution Institution { get; set; }
        public List<Picture> Pictures { get; set; }
        public List<Comment> Comments { get; set; }
        public bool IsCommented { get; set; }
        public string Email { get; set; }
    }
}
