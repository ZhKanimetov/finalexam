﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;

namespace FinalExam.ViewModels
{
    public class IndexModel
    {
        public PageViewModel PageViewModel { get; set; }
        public List<Institution> Institutions { get; set; }
        public string Search { get; set; }
    }
}
