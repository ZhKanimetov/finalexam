﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FinalExam.Models;
using FinalExam.Repositories;
using FinalExam.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FinalExam.Controllers
{
    public class HomeController : Controller
    {
        private readonly IInstitutionRepository institutionRepository;
        private readonly UserManager<User> userManager;
        private readonly IPictureRepository pictureRepository;
        private readonly ICommentRepository commentRepository;

        public HomeController(IInstitutionRepository institutionRepository,
            UserManager<User> userManager,
            IPictureRepository pictureRepository,
            ICommentRepository commentRepository)
        {
            this.institutionRepository = institutionRepository;
            this.userManager = userManager;
            this.pictureRepository = pictureRepository;
            this.commentRepository = commentRepository;
        }

        public  IActionResult Index(string search, int page=1)
        {
            int pageSize = 5;
            List<Institution> institutions = institutionRepository.Get().ToList();
            if (!String.IsNullOrEmpty(search))
            {
                institutions = institutions.Where(i => i.Name.ToLower().Contains(search.ToLower()) ||
                                                       i.Description.ToLower().Contains(search.ToLower())).ToList();
            }
            var count = institutions.Count();
            var items = institutions.Skip((page - 1) * pageSize).Take(pageSize);
            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexModel model = new IndexModel()
            {
                Institutions = items.ToList(),
                PageViewModel = pageViewModel,
                Search = search
            };
            return View(model);
        }

        public IActionResult CreateInstitution()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateInstitution(IstitutionModel model)
        {
            if (ModelState.IsValid)
            {
                User user = userManager.GetUserAsync(User).Result;
                model.UserId = user.Id;
                institutionRepository.Create(model);
            }
            else
            {
                ModelState.AddModelError("", "Неправильный логин и (или) пароль");
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Info(int institutionId)
        {
            Institution institution = institutionRepository.FindById(institutionId);
            List<Picture> pictures = pictureRepository.Get(p => p.InstitutionId == institutionId).ToList();
            User user = await userManager.GetUserAsync(User);

            InfoModel model = new InfoModel()
            {
                Institution = institution,
                Pictures = pictures,
                Comments = commentRepository.Get(c => c.InstitutionId == institutionId).ToList(),
                IsCommented = user == null ? true : commentRepository.CheckComment(institutionId, user.Id),
                Email = user == null ? "" : user.Email
            };
            return View(model);
        }


        public IActionResult AddComment(int institutionId)
        {
            ViewBag.InstitutionId = institutionId;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(CommentModel model)
        {
            User user = await userManager.GetUserAsync(User);
            commentRepository.Create(new Comment()
            {
                UserId = user.Id,
                Content = model.Content,
                InstitutionId = model.InstitutionId,
                Rating = model.Rating
            });
            institutionRepository.UpdateRating(model.InstitutionId);

            return RedirectToAction("Info", new {institutionId = model.InstitutionId});
        }


        public IActionResult DeleteComment(int institutionId, int commentId)
        {
            Comment comment = commentRepository.FindById(commentId);
            commentRepository.Remove(comment);
            institutionRepository.UpdateRating(institutionId);
            return RedirectToAction("Info", new { institutionId });
        }

        public async Task<IActionResult> AddPhoto(PhotoModel model)
        {
            User user = await userManager.GetUserAsync(User);
            model.UserId = user.Id;
            institutionRepository.AddPicture(model);
            return RedirectToAction("Info", new { institutionId = model.InstitutionId });
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
