﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class Institution : Entity
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public string PhotoPath { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; } = 5;

        [NotMapped] public int PhotoCount { get; set; }
        [NotMapped] public int CommentCount { get; set; }
    }
}
