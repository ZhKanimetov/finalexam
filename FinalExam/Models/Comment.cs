﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FinalExam.Models
{
    public class Comment : Entity
    {
        public string Content { get; set; }
        public int InstitutionId { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public int Rating { get; set; }

        public User User { get; set; }
    }
}
