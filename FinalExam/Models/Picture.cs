﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class Picture : Entity
    {
        public int InstitutionId { get; set; }
        public string UserId { get; set; }
        public string Path { get; set; }
    }
}
