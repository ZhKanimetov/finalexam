﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FinalExam.Models;
using FinalExam.Repositories;
using FinalExam.Repository;
using FinalExam.Services;
using FinalExam.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace FinalExam.Repositories
{
    public interface IInstitutionRepository : IRepository<Institution>
    {
        void Create(IstitutionModel item);
        void AddPicture(PhotoModel model);
        void UpdateRating(int institutionId);
        Institution FindByName(string name);
    }

    public class InstitutionRepository : Repository<Institution>, IInstitutionRepository
    {
        private readonly FileUploadService fileUploadService;
        private readonly IPictureRepository pictureRepository;
        private readonly ICommentRepository commentRepository;

        public InstitutionRepository(ApplicationContext context,
            FileUploadService fileUploadService,
            IPictureRepository pictureRepository,
            ICommentRepository commentRepository) : base(context)
        {
            AllEntities = context.Institutions;
            this.fileUploadService = fileUploadService;
            this.pictureRepository = pictureRepository;
            this.commentRepository = commentRepository;
        }

        

        public void Create(IstitutionModel item)
        {
            Institution institution = Create(new Institution()
            {
                Description = item.Description,
                Name = item.Name,
                PhotoPath = $"images/{item.Name}/{item.MainPhoto.FileName}",
                UserId = item.UserId
            });
            pictureRepository.Create(new Picture()
            {
                UserId = item.UserId,
                InstitutionId = institution.Id,
                Path = institution.PhotoPath
            });
            fileUploadService.AddPhoto(item.MainPhoto, item.Name);
        }

        public void AddPicture(PhotoModel model)
        {
            Institution institution = FindById(model.InstitutionId);
            pictureRepository.Create(new Picture()
            {
                InstitutionId = model.InstitutionId,
                UserId = model.UserId,
                Path = $"images/{institution.Name}/{model.File.FileName}"
            });
            fileUploadService.AddPhoto(model.File, institution.Name);
        }

        public void UpdateRating(int institutionId)
        {
            Institution institution = FindById(institutionId);
            institution.Rating = commentRepository.GetRating(institutionId);
            Update(institution);
        }

        public Institution FindByName(string name)
        {
            return AllEntities.FirstOrDefault(i => i.Name == name);
        }

        public override IEnumerable<Institution> Get()
        {
            List<Institution> institutions = AllEntities.ToList();
            foreach (var institution in institutions)
            {
                institution.PhotoCount = pictureRepository.Get(p => p.InstitutionId == institution.Id).Count();
                institution.CommentCount = commentRepository.Get(c => c.InstitutionId == institution.Id).Count();
            }

            return institutions;
        }
    }
}
