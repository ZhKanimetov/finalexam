﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;
using FinalExam.Repository;
using Microsoft.EntityFrameworkCore;

namespace FinalExam.Repositories
{
    public interface ICommentRepository : IRepository<Comment>
    {
        bool CheckComment(int institutionId, string userId);
        int GetRating(int institutionId);
    }

    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Comments;
        }

        public override IEnumerable<Comment> Get(Func<Comment, bool> predicate)
        {
            return AllEntities.AsNoTracking().Include(c => c.User).Where(predicate).ToList();
        }

        public bool CheckComment(int institutionId, string userId)
        {
            Comment comment = AllEntities.FirstOrDefault(c => c.InstitutionId == institutionId && c.UserId == userId);
            return comment != null;
        }

        public int GetRating(int institutionId)
        {
            List<Comment> comments = AllEntities.Where(i => i.InstitutionId == institutionId).ToList();
            if (!comments.Any())
            {
                return 5;
            }

            return (int)Math.Ceiling(AllEntities.Where(i => i.InstitutionId == institutionId).Average(e => e.Rating));
        }
    }
}
