﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;
using Microsoft.EntityFrameworkCore;

namespace FinalExam.Repository
{
    public interface IRepository<T> where T : Entity
    {
        T Create(T item);
        T FindById(int id);
        IEnumerable<T> Get();
        IEnumerable<T> Get(Func<T, bool> predicate);
        void Remove(T item);
        void Update(T item);
    }

    public class Repository<T> : IRepository<T> where T : Entity
    {
        private ApplicationContext context { get; set; }
        protected DbSet<T> AllEntities { get; set; }

        public Repository(ApplicationContext context)
        {
            this.context = context;
        }

        public virtual IEnumerable<T> Get()
        {
            return AllEntities.AsNoTracking().ToList();
        }

        public virtual IEnumerable<T> Get(Func<T, bool> predicate)
        {
            return AllEntities.AsNoTracking().Where(predicate).ToList();
        }
        public T FindById(int id)
        {
            return AllEntities.Find(id);
        }

        public T Create(T item)
        {
            T entity = AllEntities.Add(item).Entity;
            context.SaveChanges();
            return entity;
        }
        public void Update(T item)
        {
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Remove(T item)
        {
            AllEntities.Remove(item);
            context.SaveChanges();
        }
    }
}
