﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalExam.Models;
using FinalExam.Repository;
using FinalExam.Services;
using FinalExam.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace FinalExam.Repositories
{
    public interface IPictureRepository : IRepository<Picture>
    {
    }

    public class PictureRepository : Repository<Picture>, IPictureRepository
    {
        private readonly UserManager<User> userManager;
        private readonly FileUploadService fileUploadService;

        public PictureRepository(ApplicationContext context,
            FileUploadService fileUploadService) : base(context)
        {
            AllEntities = context.Pictures;
            this.fileUploadService = fileUploadService;
        }
    }
}
